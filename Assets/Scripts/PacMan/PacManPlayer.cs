﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManPlayer : MonoBehaviour {
    [SerializeField]
    private float speed = 3; 
    private Rigidbody2D rb;
    private Animator anim; 

	// Use this for initialization
	void Start () {
        this.rb = this.GetComponent<Rigidbody2D>();
        this.anim = this.GetComponent<Animator>();
        this.anim.speed = 0; 
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxisRaw("Horizontal") != 0)
        {
            this.anim.speed = 1; 
            rb.velocity = Vector2.right * Input.GetAxisRaw("Horizontal") * speed;
            this.transform.eulerAngles = new Vector3(0, 0, rb.velocity.x > 0 ? 0 : 180); 
        }
        else if (Input.GetAxisRaw("Vertical")!= 0)
        {
            this.anim.speed = 1; 
            rb.velocity = Vector2.up * Input.GetAxisRaw("Vertical") * speed;
            this.transform.eulerAngles = new Vector3(0, 0, rb.velocity.y > 0 ? 90 : 270);
        }

        else if (rb.velocity.x != 0 && rb.velocity.y != 0)
        {
            if (Mathf.Abs(rb.velocity.x) > Mathf.Abs(rb.velocity.y))
            {
                rb.velocity = new Vector2((rb.velocity.x/Mathf.Abs(rb.velocity.x)) * speed, 0);
            }
            else
            {
                rb.velocity = new Vector2(0, (rb.velocity.y/Mathf.Abs(rb.velocity.y)) * speed); 
            }
        }
	}



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Good")
        {
            SoundManager.Instance.PlayCoin(); 
            GameObject.FindObjectOfType<PacManController>().GivePoint(this.transform.position, collision.gameObject);
            Destroy(collision.gameObject); 
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bad")
        {
            GameObject.FindObjectOfType<PacManController>().End();
        }
    }

}

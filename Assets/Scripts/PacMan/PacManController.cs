﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManController : GuestGameController {

    [SerializeField]
    private GameObject onePoint, pacmanEnemy;
    private List<GameObject> points;
    private float timer = 0;

    [SerializeField]
    private float timeToSpawn = 5; 

    private bool startedGame = false; 
	void Start () {
        points = new List<GameObject>(GameObject.FindGameObjectsWithTag("Good"));
        int loop = points.Count - 10; 
        for (int i = 0; i< loop; i++)
        {
            int rand = Random.Range(0, points.Count);
            points[rand].SetActive(false);
            Debug.Log(points[rand].name); 
            points.RemoveAt(rand); 

        }
	}
	
	void Update () {
		if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            startedGame = true; 
        }
        if (startedGame)
        {
            timer += Time.deltaTime;
            if (timer > timeToSpawn)
            {
                timer = 0;
                Instantiate(pacmanEnemy, this.transform.position, Quaternion.identity, this.transform); 
            }
        }
	}

    public void End()
    {
        SendAttackPoints(); 
    }
    public void GivePoint(Vector3 position, GameObject point)
    {
        Instantiate(onePoint, position, Quaternion.identity);
        points.Remove(point); 
        if (points.Count <= 0)
        {
            Debug.Log("ACABA EL GAME");
            End(); 
        }
        attackPoints++; 
    }
}

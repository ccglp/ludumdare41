﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManEnemy : MonoBehaviour {
    [SerializeField]
    private float speed = 4f;
    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = Random.Range(0, 100) < 50 ? Vector2.right * speed : Vector2.left * speed; 
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Turn")
        {
            int rand = Random.Range(0, 1000);
            if (rand < 250)
            {
                rb.velocity = Vector2.left * speed; 
            }
            else if (rand < 500)
            {
                rb.velocity = Vector2.right * speed;
            }
            else if (rand < 750)
            {
                rb.velocity = Vector2.up * speed;
            }
            else
            {
                rb.velocity = Vector2.down * speed;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBall : MonoBehaviour {

    [SerializeField]
    private AnimationCurve speed;
    private float timer = 0; 
    Rigidbody2D rb;
    private bool firstTry = false;
    private Vector3 originalPosition; 
	// Use this for initialization
	void Start () {
        this.rb = this.GetComponent<Rigidbody2D>();
        originalPosition = this.transform.position; 
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxisRaw("Vertical") != 0 && !firstTry )
        {
            firstTry = true;
            TossBallFromcenter(); 
        }
        if (firstTry)
        {
            timer += Time.deltaTime; 

        }

	}


    private void TossBallFromcenter()
    {
        this.transform.position = originalPosition; 
        this.rb.velocity = new Vector2(Random.Range(-1, 1f), Random.Range(-0.5f, 0.5f)).normalized;
        this.rb.velocity *= speed.Evaluate(timer); 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ENTERED"); 
        if (other.gameObject.tag == "Bad")
        {
            GameObject.FindObjectOfType<PongController>().EndGame();
            SoundManager.Instance.PlayLaser(); 
        }
        else if (other.gameObject.tag == "Good")
        {
            GameObject.FindObjectOfType<PongController>().GivePoint();
            TossBallFromcenter();
            SoundManager.Instance.PlayCoin(); 
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SoundManager.Instance.PlayHit(); 
       // this.rb.velocity = this.rb.velocity.normalized * speed.Evaluate(timer); 
       if (collision.gameObject.tag == "Player")
        {
            int modX = this.transform.localPosition.x < 0 ? 1 : -1;
            Vector2 contact = Vector2.zero;
            int i; 
            for (i = 0; i < collision.contacts.Length; i++)
            {
                contact += collision.contacts[i].point;
            }
            contact /= i; 
            if (contact.y > collision.gameObject.transform.position.y + 0.2f)
            {
                rb.velocity = new Vector2(modX,1).normalized * speed.Evaluate(timer); 
            }
            else if (contact.y < collision.gameObject.transform.position.y - 0.2f)
            {
                rb.velocity = new Vector2(modX, -1).normalized * speed.Evaluate(timer); 
            }
            else
            {
                rb.velocity = new Vector2(modX, 0).normalized * speed.Evaluate(timer); 
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongController : GuestGameController {
    [SerializeField]
    private GameObject plusOne; 

	public void EndGame()
    {
        SendAttackPoints(); 
    }

    public void GivePoint()
    {
        Instantiate(plusOne, this.transform.position, Quaternion.identity);
        attackPoints++; 
    }
}

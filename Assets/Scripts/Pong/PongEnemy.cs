﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongEnemy : MonoBehaviour {
    [SerializeField]
    private GameObject ball;
    [SerializeField]
    private float speed = 8;
    private Rigidbody2D rb; 
	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody2D>(); 
	}

    // Update is called once per frame
    void Update() {
        if (ball.transform.position.y > this.transform.position.y + 0.5f)
        {
            this.rb.velocity = Vector2.up * speed; 
        }
        else if (ball.transform.position.y < this.transform.position.y - 0.5f)
        {
            this.rb.velocity = Vector2.down * speed; 
        }
        else
        {
            this.rb.velocity = Vector2.zero; 
        }
	}
}

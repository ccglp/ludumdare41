﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongPlayer : MonoBehaviour {
    [SerializeField]
    private float speed = 10;

    private Rigidbody2D rb; 
	// Use this for initialization
	void Start () {
        this.rb = this.GetComponent<Rigidbody2D>(); 
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector2(0, speed * Input.GetAxisRaw("Vertical"));
        if (this.transform.localPosition.y > 3.7f)
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, 3.69f, this.transform.localPosition.z); 
        }
        else if (this.transform.localPosition.y < -3.7f)
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, -3.69f, this.transform.localPosition.z);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class UpAndDown : MonoBehaviour {

    [SerializeField]
    private float upOffset = 40, timeToLoop = 0.5f;
    private int mod = -1;
    private RectTransform rt;
	// Use this for initialization
	void Start () {
        rt = this.GetComponent<RectTransform>();
        Loop(); 
    }

    private void Loop()
    {
        mod *= -1;
        this.rt.DOAnchorPosY(this.rt.anchoredPosition.y + upOffset * mod, timeToLoop).onComplete = () =>
         {
             Loop(); 
         };
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

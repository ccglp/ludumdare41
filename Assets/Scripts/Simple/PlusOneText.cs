﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening; 
public class PlusOneText : MonoBehaviour {

    TextMeshPro text;
    [SerializeField]
    private float timeToLive = 1f; 
	// Use this for initialization
	void Start () {
        text = this.GetComponent<TextMeshPro>();
        this.transform.DOMoveY(this.transform.position.y+ 2, timeToLive).onComplete = ()=>
        {
            Destroy(this.gameObject); 
        };
        text.DOFade(0, timeToLive); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening; 

public class FeedbackText : MonoBehaviour {
    [SerializeField]
    private Sprite defense, attack;
    [SerializeField]
    private float fadeTime;
    [SerializeField]
    private Color defenseColor, attackColor; 

    private int number;
    private bool attacking = false; 
    private TextMeshPro text;
    private SpriteRenderer rend; 

  

    // Use this for initialization
    void Start () {
        text = this.GetComponentInChildren<TextMeshPro>();
        rend = this.GetComponentInChildren<SpriteRenderer>();
        rend.sprite = attacking ? attack : defense;
        text.color = attacking ? attackColor : defenseColor;
        text.text = number.ToString();


        this.transform.DOMoveY(this.transform.position.y + 2, fadeTime).onComplete = () =>
        {
            Destroy(this.gameObject);
        };
        text.DOFade(0, fadeTime);
        rend.DOFade(0, fadeTime); 
    }
	

    public int Number
    {
        get
        {
            return number;
        }

        set
        {
            number = value;
        }
    }

    public bool Attacking
    {
        get
        {
            return attacking;
        }

        set
        {
            attacking = value;
        }
    }
}

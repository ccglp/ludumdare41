﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> { 
    [SerializeField]
    private AudioSource select, hit, jump, laser, coin; 
	

    public void PlayJump()
    {
        jump.Play();
    }
	

    public void PlayHit()
    {
        hit.Play(); 
    }

    public void PlayLaser()
    {
        laser.Play(); 
    }

    public void PlayCoin()
    {
        coin.Play(); 
    }
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            select.Play(); 
        }
	}
}

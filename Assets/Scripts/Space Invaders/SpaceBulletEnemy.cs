﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceBulletEnemy : MonoBehaviour {
    private Rigidbody2D rb;
    [SerializeField]
    private float speed = 20;

    // Use this for initialization
    void Start()
    {
        this.rb = this.GetComponent<Rigidbody2D>();
        this.rb.velocity = new Vector2(0, -speed);
    }


    
}

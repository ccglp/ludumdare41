﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacePlayer : MonoBehaviour {
    [SerializeField]
    private float speed = 7;
    [SerializeField]
    private GameObject bullet; 
    private Rigidbody2D rb;
    [SerializeField]
    private float coolDown = 1;
    private float timer = 1; 
	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody2D>(); 
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime; 
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speed, 0);
        if (this.transform.localPosition.x < -10)
        {
            rb.velocity = Vector2.zero;
            this.transform.localPosition = new Vector3(-9.99f, this.transform.localPosition.y); 

        }
        else if (this.transform.localPosition.x > 10)
        {
            rb.velocity = Vector2.zero;
            this.transform.localPosition = new Vector3(9.99f, this.transform.localPosition.y);
        }

        if (Input.GetButtonDown("Jump") && timer > coolDown)
        {
            timer = 0; 
            Instantiate(bullet, this.transform.position, Quaternion.identity, this.transform.parent); 
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bad" || collision.gameObject.tag == "Die")
        {
            GameObject.FindObjectOfType<SpaceInvadersController>().EndGame();
        }
    }
}

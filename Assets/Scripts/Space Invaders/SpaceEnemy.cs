﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceEnemy : MonoBehaviour {
    [SerializeField]
    private AnimationCurve speed, spawnBullet;
    [SerializeField]
    private GameObject bullet; 
    private float timer,timerBullet;
    private Rigidbody2D rb;
    private int mod = 1; 
    void Start () {
        rb = this.GetComponent<Rigidbody2D>();
        timerBullet = Random.Range(0, spawnBullet.Evaluate(0) - 1); 
	}


    private void LateUpdate()
    {
        this.transform.position += new Vector3(speed.Evaluate(timer) * mod, 0) * Time.deltaTime;
    }
    void Update () {
        timer += Time.deltaTime;
        timerBullet += Time.deltaTime; 
        if (timerBullet > spawnBullet.Evaluate(timer))
        {
            timerBullet = 0; 
            Instantiate(bullet, this.transform.position, Quaternion.identity, this.transform.parent); 
        }
        

        if (this.transform.localPosition.x < -10)
        {
            var spaces = GameObject.FindObjectsOfType<SpaceEnemy>();
            for (int i = 0; i< spaces.Length; i++)
            {
                spaces[i].CallRight(); 
            }
        }

        else if (this.transform.localPosition.x > 10)
        {
            var spaces = GameObject.FindObjectsOfType<SpaceEnemy>();
            for (int i = 0; i < spaces.Length; i++)
            {
                spaces[i].CallLeft();
            }
        }
    }

    public void CallRight()
    {
        if (mod == -1)
        {
            mod = 1;
            this.transform.position -= Vector3.up; 
        }
    }

    public void CallLeft()
    {
        if (mod == 1)
        {
            mod = -1;
            this.transform.position -= Vector3.up; 
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 
public class SpaceInvadersController : GuestGameController {
    [SerializeField]
    private GameObject onePlus;
    private int numberOfAliens = 0; 

    private void Start()
    {
        numberOfAliens = GameObject.FindObjectsOfType<SpaceEnemy>().Length; 
    }
    public void EndGame()
    {
        SendAttackPoints(); 
    }

    public void CallShot(Vector3 position)
    {
        Instantiate(onePlus, position, Quaternion.identity); 
        attackPoints++;
        numberOfAliens--; 
        if (numberOfAliens <= 0)
        {
            EndGame(); 
        }

        
    }


}

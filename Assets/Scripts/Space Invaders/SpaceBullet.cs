﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceBullet : MonoBehaviour {
    private Rigidbody2D rb;
    [SerializeField]
    private float speed = 20; 
    
	// Use this for initialization
	void Start () {
        this.rb = this.GetComponent<Rigidbody2D>();
        this.rb.velocity = new Vector2(0, speed);
        SoundManager.Instance.PlayLaser(); 
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Die")
        {
            GameObject.FindObjectOfType<SpaceInvadersController>().CallShot(this.transform.position);
            SoundManager.Instance.PlayCoin(); 
            Destroy(collision.gameObject);
            Destroy(this.transform.gameObject);
            
        }
    }
}

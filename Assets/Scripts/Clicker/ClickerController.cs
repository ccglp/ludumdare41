﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class ClickerController : GuestGameController {
    [SerializeField]
    private float timeToClick = 10;
    [SerializeField]
    private GameObject plusOne; 
    private GameObject cookie;
    private float originalCookieScale; 
    private TextMeshPro timerText, clickText;
    private float timer;
    private int clickCounter = 0; 
    private bool gameStarted = false; 

	void Start () {
        cookie = this.transform.Find("Cookie").gameObject;
        timerText = this.transform.Find("TimerText").GetComponent<TextMeshPro>();
        clickText = this.transform.Find("ClickerText").GetComponent<TextMeshPro>();
        originalCookieScale = cookie.transform.localScale.x; 
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            gameStarted = true;
            cookie.transform.localScale = Vector3.one * originalCookieScale * 0.9f;
            clickCounter++;
            clickText.text = clickCounter.ToString() + " clicks"; 
            if (clickCounter % 10 == 0)
            {
                attackPoints++; 
            }
            Instantiate(plusOne, cookie.transform.position, Quaternion.identity); 
        }

        if (gameStarted)
        {
            timer += Time.deltaTime;
            timerText.text = ((int) (timeToClick- timer)).ToString() + "s";
            if (timer > timeToClick)
            {
                SendAttackPoints(); 
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            cookie.transform.localScale = Vector3.one * originalCookieScale; 
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidBall : MonoBehaviour {


    [SerializeField]
    private AnimationCurve speed;
    private float timer = 0;
    Rigidbody2D rb;
    private bool firstTry = false;
    private Vector3 originalPosition;
    // Use this for initialization
    void Start()
    {
        this.rb = this.GetComponent<Rigidbody2D>();
        originalPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 && !firstTry)
        {
            firstTry = true;
            TossBallFromcenter();
        }
        if (firstTry)
        {
            timer += Time.deltaTime;

        }

    }


    private void TossBallFromcenter()
    {
        this.transform.position = originalPosition;
        this.rb.velocity = new Vector2(Random.Range(-0.2f, 0.2f), Random.Range(-1f, 1f)).normalized;
        this.rb.velocity *= speed.Evaluate(timer);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      
        if (other.gameObject.tag == "Bad")
        {
            GameObject.FindObjectOfType<ArkanoidGameController>().EndGame();
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
        
    {
        SoundManager.Instance.PlayHit(); 
        if (collision.gameObject.tag == "Good")
        {
            GameObject.FindObjectOfType<ArkanoidGameController>().GivePoint(collision.transform.position);
            Destroy(collision.gameObject);
            SoundManager.Instance.PlayCoin(); 
        }
       
        else if (collision.gameObject.tag == "Player")
        {
            Vector2 contact = Vector2.zero;
            contact = collision.contacts[0].point; 
            if (contact.x > collision.gameObject.transform.position.x + 0.2f)
            {
                rb.velocity = new Vector2(0.5f, 1).normalized * speed.Evaluate(timer);
            }
            else if (contact.x < collision.gameObject.transform.position.x - 0.2f)
            {
                rb.velocity = new Vector2(-0.5f, 1).normalized * speed.Evaluate(timer);
            }
            else
            {
                rb.velocity = new Vector2(0, 1).normalized * speed.Evaluate(timer);
            }
        }
    }
}

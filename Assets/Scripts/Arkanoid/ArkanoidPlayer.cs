﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidPlayer : MonoBehaviour {

    [SerializeField]
    private float speed;
    private Rigidbody2D rb;


    void Start () {
        this.rb = this.GetComponent<Rigidbody2D>(); 
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speed, 0); 
        if (this.transform.localPosition.x < -10)
        {
            this.transform.localPosition = new Vector3(-9.9f, this.transform.localPosition.y, 0); 
        }
        else if (this.transform.localPosition.x > 10)
        {
            this.transform.localPosition = new Vector3(9.9f, this.transform.localPosition.y, 0); 
        }
	}
}

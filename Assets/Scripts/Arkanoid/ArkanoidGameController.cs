﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidGameController : GuestGameController {
    [SerializeField]
    private GameObject plusOne;
    private int score = 0;
    private int numberOfBricks; 

    private void Start()
    {
        numberOfBricks = GameObject.FindGameObjectsWithTag("Good").Length; 
    }
    public void EndGame()
    {
        SendAttackPoints();
    }

    public void GivePoint(Vector3 position)
    {
        Instantiate(plusOne, position, Quaternion.identity);
        score++; 
        if (score >= 2)
        {
            score = 0;
            attackPoints++; 
        }
        numberOfBricks--; 
        if (numberOfBricks <= 0)
        {
            EndGame(); 
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public enum SquareType
{
    NORMAL,
    FOREST,
    MOUNTAIN
}


public class Square : MonoBehaviour {

    private SquareType type; 
    private Coordinate mapPosition;
    private Collider2D coll; 
    private SpriteRenderer rend;
    [SerializeField]
    private Color highlightedColor;
    private Color originalColor; 
    [SerializeField]
    private List<SquareSpritePar> spriteTypeRelations; 

    void Start () {
        if (coll == null)
        {
            coll = this.GetComponent<Collider2D>();
        }
        rend = this.GetComponent<SpriteRenderer>();
        originalColor = rend.color; 
	}
	
    public void InitSquare(Coordinate coord)
    {
        this.mapPosition = coord; 
    }

    public void SetSpriteType()
    {
        if (rend == null) rend = this.GetComponent<SpriteRenderer>();
        rend.sprite = spriteTypeRelations.Find(x => x.type == type).sprite; 
    }

    private void OnMouseEnter()
    {
        rend.color = highlightedColor;
        GameController.Instance.SquareHoverCallback(this); 
    }

    private void OnMouseExit()
    {
        rend.color = originalColor; 
    }



    private void OnMouseDown()
    {
        Debug.Log("Holi, soy  la square: " + mapPosition.ToString());
        GameController.Instance.SquareClickCallback(this); 
    }

    public void DeactivateCollider()
    {
        if (coll ==null)
        {
            coll = this.GetComponent<Collider2D>(); 
        }
        coll.enabled = false;
    }

    public void ActivateCollider()
    {
        coll.enabled = true; 
    }

    public Coordinate MapPosition
    {
        get
        {
            return mapPosition;
        }
    }

    public SquareType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public SpriteRenderer Rend
    {
        get
        {
            return rend;
        }

      
    }
}

[System.Serializable]
public struct Coordinate
{
    public int x, y; 
    public Coordinate(int x, int y)
    {
        this.x = x;
        this.y = y; 
    }

    public int Distance(Coordinate other)
    {
        int x = Mathf.Abs(other.x - this.x);
        int y = Mathf.Abs(other.y - this.y);
        return x + y; 
    }

    public override string ToString()
    {
        return "(" + x.ToString() + "," + y.ToString() + ")";
    }

}

[System.Serializable]
public class SquareSpritePar
{
    public SquareType type;
    public Sprite sprite;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board
{
    public Square[,] squares;
    public List<Square> squaresNotValid; 
    public Board(Square[,] squares)
    {
        this.squares = squares;
        this.squaresNotValid = new List<Square>(); 
    }

    public Square GetRandomSquare()
    {
        return squares[Random.Range(0, squares.GetLength(0)), Random.Range(0, squares.GetLength(1))]; 
    }

    public Square GetCharacterPlacementSquare()
    {
        Square square = squares[Random.Range(squares.GetLength(0)/2, squares.GetLength(0)), Random.Range(0, squares.GetLength(1))];
        while (squaresNotValid.Find(x=>x.MapPosition.x == square.MapPosition.x && x.MapPosition.y == square.MapPosition.y) != null)
        {
            square = squares[Random.Range(squares.GetLength(0) / 2, squares.GetLength(0)), Random.Range(0, squares.GetLength(1))];
        }
        squaresNotValid.Add(square);
        return square;
    }


    public Square GetEnemyPlacementSquare()
    {
        Square square = squares[Random.Range(0, squares.GetLength(0) / 2), Random.Range(0, squares.GetLength(1))];
        while (squaresNotValid.Find(x => x.MapPosition.x == square.MapPosition.x && x.MapPosition.y == square.MapPosition.y) != null)
        {
            square = squares[Random.Range(0, squares.GetLength(0) / 2), Random.Range(0, squares.GetLength(1))];
        }
        squaresNotValid.Add(square);
        return square;
    }


    public Square GetSquareFromPosition(Vector3 position)
    {
        for (int i = 0; i< squares.GetLength(0); i++)
        {
            for (int j = 0; j< squares.GetLength(1); j++)
            {
                if (Vector3.Distance(position, squares[i,j].transform.position) < 0.001f)
                {
                    return squares[i, j];
                }
            }
        }
        return null; 
    }


    public List<Square> GetAdyacentSquaresFrom(Square square)
    {
        List<Square> list = new List<Square>(); 
        if (square.MapPosition.x -1 >= 0)
        {
            list.Add(squares[square.MapPosition.x - 1, square.MapPosition.y]);
        }
        if (square.MapPosition.x +1 < squares.GetLength(0))
        {
            list.Add(squares[square.MapPosition.x + 1, square.MapPosition.y]);
        }

        if (square.MapPosition.y -1 >= 0)
        {
            list.Add(squares[square.MapPosition.x, square.MapPosition.y - 1]);
        }

        if (square.MapPosition.y +1 < squares.GetLength(1))
        {
            list.Add(squares[square.MapPosition.x, square.MapPosition.y + 1]);
        }

        return list; 
    }

}
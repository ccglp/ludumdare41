﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; 
public class Enemy : MonoBehaviour {

    private SpriteRenderer rend;
    private Square actualSquare;

    [SerializeField]
    private int speed = 5;
    [SerializeField]
    private int life = 10;
    private int lifeTotal;

    [SerializeField]
    private string charName;


    private void Start()
    {
        rend = this.GetComponent<SpriteRenderer>();
        lifeTotal = life; 
    }


    private void OnMouseEnter()
    {
      

        CanvasController.Instance.SetTopBar(rend, CharName, lifeTotal, life, this.actualSquare.Type, this.actualSquare.Rend.sprite);
    }

    public Square ActualSquare
    {
        get
        {
            return actualSquare;
        }

        set
        {
            if (actualSquare != null)
            {
                if ((from enemy in GameObject.FindObjectsOfType<Enemy>()
                     where enemy.actualSquare.MapPosition.x == this.actualSquare.MapPosition.x &&
enemy.actualSquare.MapPosition.y == this.actualSquare.MapPosition.y
                     select enemy.actualSquare).ToArray().Length == 1)
                {
                    actualSquare.ActivateCollider();
                }
            }
            actualSquare = value;
            actualSquare.DeactivateCollider();
        }
    }

    public int Speed
    {
        get
        {
            return speed;
        }

    }

    public int Life
    {
        get
        {
            return life;
        }

        set
        {
            life = value;
        }
    }

    public SpriteRenderer Rend
    {
        get
        {
            return rend;
        }

      
    }

    public string CharName
    {
        get
        {
            return charName;
        }

       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

[System.Serializable]
public enum CharacterType
{
    FLAPPY,
    PACMAN,
    CLICKER,
    SPACER,
    PONG,
    ARKANOID
    
}

public class Character : MonoBehaviour {
    [SerializeField]
    private List<TypeAnimatorPar> charactersList; 
    [SerializeField]
    private CharacterType type;
    private SpriteRenderer rend; 
    private Square actualSquare;
    private Animator anim; 
    [SerializeField]
    private int speed = 5;
    [SerializeField]
    private int life = 10;

    [SerializeField]
    private string charName;
    

    private int lifeTotal; 


    public bool movementTurn = false, attackTurn = false;


   
    void Start () {
        anim = this.GetComponent<Animator>(); 

        charName = type.ToString();
        anim.runtimeAnimatorController = charactersList.Find(x => x.type == this.type).animator; 
        rend = this.GetComponent<SpriteRenderer>();
        lifeTotal = life;
        anim.speed = 0;
        DOVirtual.DelayedCall(0.2F, () =>
        {
            CanvasController.Instance.SetTopBar(rend, CharName, lifeTotal, life, this.actualSquare.Type, this.actualSquare.Rend.sprite);

        });
    }



    private void OnMouseDown()
    {
        Debug.Log("Holi, estoy en la square: " + actualSquare.MapPosition.ToString());
        GameController.Instance.CharacterClicked(this);
        anim.speed = 1; 
    }

    private void OnMouseEnter()
    {
        if (!movementTurn)
        {
            rend.color = new Color(0.8f, 0.8f, 0.8f);
            if (anim.speed != 1)
                anim.speed = 0.4f;
        }

        CanvasController.Instance.SetTopBar(rend, CharName, lifeTotal, life, this.actualSquare.Type, this.actualSquare.Rend.sprite);
        
    }

    private void OnMouseExit()
    {
        if (!movementTurn)
        {
            ResetColor();
            if (anim.speed != 1)
                anim.speed = 0;
        }
    }


    public void ResetColor()
    {
        rend.color = Color.white; 
       
    }

    public void EndColorTurn()
    {
        rend.color = new Color(0.6f, 0.6f, 0.6f);
        anim.speed = 0; 
    }

    public CharacterType Type
    {
        get
        {
            return type;
        }

        set
        {
            this.type = value; 
        }

    }

    public Square ActualSquare
    {
        get
        {
            return actualSquare;
        }

        set
        {
            if (actualSquare != null)
            {
                actualSquare.ActivateCollider(); 
            }
            actualSquare = value;
            actualSquare.DeactivateCollider(); 
        }
    }

    public int Speed
    {
        get
        {
            return speed;
        }

    }

    public int Life
    {
        get
        {
            return life;
        }

        set
        {
            life = value;
        }
    }

    public string CharName
    {
        get
        {
            return charName;
        }

        set
        {
            charName = value;
        }
    }

    public SpriteRenderer Rend
    {
        get
        {
            return rend;
        }

    }

  
}


[System.Serializable]
public class TypeAnimatorPar
{
    public CharacterType type;
    public RuntimeAnimatorController animator; 
}
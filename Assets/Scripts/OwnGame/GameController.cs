﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using UnityEngine.SceneManagement; 

public class GameController : Singleton<GameController> {

    private Board board;
    private bool actionTurn = false;
    private bool aiTurn = false; 
   

    [SerializeField]
    private GameObject characterPrefab, enemyPrefab,textFeedbackPrefab;

    [Header("Games prefabs")]
    [SerializeField]
    private GameObject flappy;
    [SerializeField]
    private GameObject pacman, clicker,space, pong,arkanoid; 


    [Header("Times")]
    [SerializeField]
    private float timeForSquareMovement = 0.1f; 

    private Character movingCharacter = null;
    private Enemy targetEnemy; 

    private List<Character> myCharacters;
    private List<Enemy> enemies;

    private List<Vector3> posiblePathMovement;

    [SerializeField]
    private int numberOfEnemies = 5, numberOfChars = 5; 
    void Start () {
    
        PlaceCharacters();
        PlaceAI(); 
	}


    private void PlaceCharacters()
    {
        myCharacters = new List<Character>();
        characterPrefab.transform.localScale = new Vector3(MapGenerator.Instance.SquareSize, MapGenerator.Instance.SquareSize, 1); 
        for (int i = 0; i < numberOfChars; i++)
        {
            Square randomSquare = board.GetCharacterPlacementSquare();
            var aux = (GameObject)Instantiate(characterPrefab, randomSquare.transform.position, Quaternion.identity);
            Character character = aux.GetComponent<Character>();
            character.Type = (CharacterType)i; 
            character.ActualSquare = randomSquare;
            myCharacters.Add(character);
        }
    }

    private void PlaceAI()
    {
        enemies = new List<Enemy>();
        enemyPrefab.transform.localScale = new Vector3(MapGenerator.Instance.SquareSize, MapGenerator.Instance.SquareSize, 1);

        for (int i = 0; i < numberOfEnemies; i++)
        {
            Square randomSquare = board.GetEnemyPlacementSquare();
            var aux = (GameObject)Instantiate(enemyPrefab, randomSquare.transform.position, Quaternion.identity);
            Enemy enemy = aux.GetComponent<Enemy>();
            enemy.ActualSquare = randomSquare;
            enemies.Add(enemy);
        }
    }




    public void SquareClickCallback(Square square)
    {
        if (movingCharacter != null && !movingCharacter.movementTurn)
        {
            if (square.MapPosition.Distance(movingCharacter.ActualSquare.MapPosition) <= movingCharacter.Speed)
            {
                movingCharacter.ActualSquare = square;
                movingCharacter.transform.DOPath(posiblePathMovement.ToArray(), timeForSquareMovement * posiblePathMovement.Count);
                LineMovement.Instance.EndTracking();
                movingCharacter.movementTurn = true;

                ActionTurnTrigger(); //ACTION CALL
            }
        }
    }

    private void ActionTurnTrigger()
    {
        actionTurn = true;
        //Para decidir si puede o no atacar, ciclo sobre la lista de enemigos y compruebo si alguna de sus squares está adyacente. Si hay mas de un enemigo el minijuego será mas difisil.
        bool canAttack = false; 
        for (int i = 0; i< enemies.Count; i++)
        {
            if (enemies[i].ActualSquare.MapPosition.Distance(movingCharacter.ActualSquare.MapPosition) == 1)
            {
                canAttack = true;
                targetEnemy = enemies[i]; 
            }
        }

        CanvasController.Instance.ShowActionPanel(canAttack);

    }


    public void SquareHoverCallback(Square square)
    {
        if (movingCharacter != null && !movingCharacter.movementTurn)
        {
            
                LineMovement.Instance.StartTrackingMovement(movingCharacter.transform.position);
                Coordinate positionOne = movingCharacter.ActualSquare.MapPosition;
                Coordinate positionTwo = square.MapPosition;
                posiblePathMovement = GetPathMovementPoints(positionOne, positionTwo,movingCharacter.Speed, true); 
                
            
           
        }
    }

    List<Vector3> GetPathMovementPoints(Coordinate origin, Coordinate destination, int limit, bool draw = false)
    {
        List<Vector3> path = new List<Vector3>(); 
        Coordinate movementsToDo = new Coordinate(destination.x - origin.x, destination.y - origin.y);
        posiblePathMovement = new List<Vector3>();

        int x = origin.x;
        int y = origin.y;
        int amount = 0; 
        for (int i = 0; i < Mathf.Abs(movementsToDo.x) && amount < limit; i++)
        {
            amount++; 
            x += movementsToDo.x / Mathf.Abs(movementsToDo.x);
            if (draw) LineMovement.Instance.AddPointToTheMovement(board.squares[x, y].transform.position);
            path.Add(board.squares[x, y].transform.position);
        }

        for (int j = 0; j < Mathf.Abs(movementsToDo.y) && amount < limit; j++)
        {
            amount++; 
            y += movementsToDo.y / Mathf.Abs(movementsToDo.y);
            if (draw) LineMovement.Instance.AddPointToTheMovement(board.squares[x, y].transform.position);
            path.Add(board.squares[x, y].transform.position);
        }

        return path; 
    }

    public void CharacterClicked(Character character)
    {

        if (character != null && !character.movementTurn && !actionTurn)
        {
            movingCharacter = character;
            LineMovement.Instance.StartTrackingMovement(character.transform.position);
        }
    }






    //Action Callbacks

    public void OnAttackButtonClick()
    {
        CanvasController.Instance.HideActionPanel(); 
        AttackTurn(); 
       
    }
    
    public void AttackTurn()
    {
        CanvasController.Instance.ShowAttackPanel(aiTurn);
        switch (movingCharacter.Type)
        {
            case CharacterType.FLAPPY:
                Instantiate(flappy);
                break;
            case CharacterType.PACMAN:
                Instantiate(pacman);
                break;
            case CharacterType.CLICKER:
                Instantiate(clicker);
                break;

            case CharacterType.SPACER:
                Instantiate(space);
                break;

            case CharacterType.PONG:
                Instantiate(pong); 
                break;
            case CharacterType.ARKANOID:
                Instantiate(arkanoid);
                break; 
        }
         //ELECCION DE PJ PARA PONER EL JUEGO. 
    }

    public void CallbackFromAttack(int attackDamage)
    {
        SoundManager.Instance.PlayHit(); 
        CanvasController.Instance.HideAttackPanel(() =>
        {
            if (!aiTurn)
            {
                AttackCallbackOnMyTurn(attackDamage);
            }
            else
            {
                AttackCallbackOnAITurn(attackDamage);
            }
        });
      
    }
    private void AttackCallbackOnAITurn(int attackDamage)
    {
        float offset = targetEnemy.transform.position.x - movingCharacter.transform.position.x;
        var feedbackDefense = ((GameObject)Instantiate(textFeedbackPrefab, movingCharacter.transform.position , Quaternion.identity)).GetComponent<FeedbackText>();
        feedbackDefense.Attacking = false;
        feedbackDefense.Number = (int)movingCharacter.ActualSquare.Type + attackDamage;

        var feedbackAttack = ((GameObject)Instantiate(textFeedbackPrefab, targetEnemy.transform.position + Vector3.right * offset, Quaternion.identity)).GetComponent<FeedbackText>();
        feedbackAttack.Attacking = true;
        feedbackAttack.Number = 10; 

        movingCharacter.Life -= Mathf.Clamp(10 - attackDamage - (int)movingCharacter.ActualSquare.Type, 0,10);
        if (movingCharacter.Life<= 0)
        {
            myCharacters.Remove(movingCharacter);
            movingCharacter.ActualSquare.ActivateCollider(); 
            Destroy(movingCharacter.gameObject); 
            if (myCharacters.Count <= 0)
            {
                EndGame(); 
            }
        }
        actionTurn = false; 
    }
    private void AttackCallbackOnMyTurn(int attackDamage)
    {
        float offset = targetEnemy.transform.position.x - movingCharacter.transform.position.x;

        var feedbackDefense = ((GameObject)Instantiate(textFeedbackPrefab, targetEnemy.transform.position + Vector3.right * offset, Quaternion.identity)).GetComponent<FeedbackText>();
        feedbackDefense.Attacking = false;
        feedbackDefense.Number = (int)targetEnemy.ActualSquare.Type;

        var feedbackAttack = ((GameObject)Instantiate(textFeedbackPrefab, movingCharacter.transform.position, Quaternion.identity)).GetComponent<FeedbackText>();
        feedbackAttack.Attacking = true;
        feedbackAttack.Number = attackDamage;

        CharacterTackleAnimation();

        targetEnemy.Life -= attackDamage - (int)targetEnemy.ActualSquare.Type;
        if (targetEnemy.Life <= 0)
        {
            enemies.Remove(targetEnemy);
            targetEnemy.ActualSquare.ActivateCollider(); 
            Destroy(targetEnemy.gameObject);
            if (enemies.Count <= 0)
            {
                EndGame(); 
            }
        }


        EndTurnAction();
    }

    private void CharacterTackleAnimation()
    {
        Character animCharacter = movingCharacter; 
        Vector3 originalCharposition = movingCharacter.transform.position;
        animCharacter.transform.DOMove(targetEnemy.transform.position, timeForSquareMovement / 3).OnComplete(() =>
        {
            animCharacter.transform.DOMove(originalCharposition, timeForSquareMovement);
        });
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }

    private void EndGame()
    {
        if (enemies.Count <= 0)
        {
            CanvasController.Instance.ShowEndGamePanel("You Win!");
        }
        else
        {
            CanvasController.Instance.ShowEndGamePanel("You Lose!"); 
        }
    }

    public void OnWaitButtonClick()
    {
        EndTurnAction(); 
    }


    private void EndTurnAction()
    {
        movingCharacter.attackTurn = true;
        movingCharacter.EndColorTurn(); 
        movingCharacter = null;
        
        actionTurn = false;
        CanvasController.Instance.HideActionPanel();
        CheckAllTurnEnd(); 
    }


    private void CheckAllTurnEnd()
    {
        for (int i = 0; i< myCharacters.Count; i++)
        {
            if (!myCharacters[i].attackTurn)
            {
                return;
            }
        }

        EndTurn(); 
    }

    private void EndTurn()
    {
        CanvasController.Instance.FadeInAndOutInterTurnPanel(true);
        DOVirtual.DelayedCall(4f, () =>
         {
             StartCoroutine(AITurn());
         });
        //Aquí ira el turno de la IA TODO
        

        //StartTurn(); // PLaceholder;
    }

    IEnumerator AITurn()
    {
        aiTurn = true; 
        List<Square> ocupedSquares = new List<Square>();
        ocupedSquares.AddRange((from square in myCharacters select square.ActualSquare).ToArray());

        for (int i = 0; i< enemies.Count; i++)
        {
            Debug.Log("Enemy turn: " + i.ToString()); 
            bool canAttack = EnemyTurn(enemies[i], ocupedSquares);
            ocupedSquares.Add(enemies[i].ActualSquare); 
            yield return new WaitForSeconds(timeForSquareMovement * 8);
            if (canAttack)
            {
                targetEnemy = enemies[i]; 
                actionTurn = true;
                AttackTurn();
                while (actionTurn)
                {
                    yield return new WaitForEndOfFrame(); 
                }
                targetEnemy = null; 
               
            }
            movingCharacter = null; 
        }


        DOVirtual.DelayedCall(2.3f, () =>
        
        {
            aiTurn = false;
        });
        
        CanvasController.Instance.FadeInAndOutInterTurnPanel(false);
        StartTurn();
        yield return null; 
    }

    private bool EnemyTurn(Enemy enemy, List<Square> ocupedSquares)
    {
        // Primero miro que personaje tiene mas cerca para atacar. 
        List<Character> myCharacters = new List<Character>(this.myCharacters);
        bool canMove = false;
        bool canAttack = false; 
        while (!canMove) {
            Character closestCharacter = myCharacters[0];
            for (int i = 1; i < myCharacters.Count; i++)
            {
                if (enemy.ActualSquare.MapPosition.Distance(closestCharacter.ActualSquare.MapPosition) > enemy.ActualSquare.MapPosition.Distance(myCharacters[i].ActualSquare.MapPosition)){
                    closestCharacter = myCharacters[i];
                }
            }


            Debug.Log("Closest character to the enemy : " + closestCharacter.ActualSquare.MapPosition.ToString()); 
            // Una vez tengo al personaje claro, tengo que mirar cual de las 4 posibilidades de casillas adyacentes está mas cerca, si es que no estan ocupadas.

            List<Square> adyacentSquares = board.GetAdyacentSquaresFrom(closestCharacter.ActualSquare);
            Square closestSquare = null; 
            for (int i = 0; i< adyacentSquares.Count; i++)
            {
                if ((closestSquare == null ||
                    enemy.ActualSquare.MapPosition.Distance(closestSquare.MapPosition) > enemy.ActualSquare.MapPosition.Distance(adyacentSquares[i].MapPosition)) 
                    && ocupedSquares.Find(x => x.MapPosition.x == adyacentSquares[i].MapPosition.x && x.MapPosition.y == adyacentSquares[i].MapPosition.y
                    ) == null){
                    Debug.Log("Closest square!"); 
                    closestSquare = adyacentSquares[i]; 
                }
            }
            if (closestSquare == null)
            {
                myCharacters.Remove(closestCharacter); 
            }
            else
            {
                canMove = true;
                posiblePathMovement = GetPathMovementPoints(enemy.ActualSquare.MapPosition, closestSquare.MapPosition, enemy.Speed);

                if (posiblePathMovement.Count !=  0)
                {
                    if (posiblePathMovement.Count > enemy.Speed)
                    {
                        posiblePathMovement = posiblePathMovement.GetRange(0, enemy.Speed);
                    }
                    enemy.ActualSquare = board.GetSquareFromPosition(posiblePathMovement[posiblePathMovement.Count - 1]);
                    enemy.transform.DOPath(posiblePathMovement.ToArray(), timeForSquareMovement * posiblePathMovement.Count);
                    if (enemy.ActualSquare.MapPosition.Distance(closestCharacter.ActualSquare.MapPosition) == 1)
                    {
                        canAttack = true; 
                    }
                }
                else
                {
                    canAttack = true; 
                }
                movingCharacter = closestCharacter;
                //Else esta parado!
            }


        }
        return canAttack;


    }

    private void StartTurn()
    {
        for (int i = 0; i < myCharacters.Count; i++)
        {
            myCharacters[i].ResetColor(); 
            myCharacters[i].movementTurn = false;
            myCharacters[i].attackTurn = false; 
        }
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !aiTurn)
        {
            CharacterClicked(myCharacters.Find(x => !x.movementTurn));
        }
    }



    public Board Board
    {
        get
        {
            return board;
        }

        set
        {
            this.board = value; 
        }

    }
}


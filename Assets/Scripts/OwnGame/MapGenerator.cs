﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : Singleton<MapGenerator> {

    [SerializeField]
    private Coordinate mapSize;
    [SerializeField]
    private float squareSize;

    [SerializeField]
    private GameObject squarePrefab;

    [Header("Cellular")]
    [SerializeField]
    private int minLimitForest;
    [SerializeField]
    private int maxLimitForest, minLimitMountain, maxLimitMountain, iterations;
    [SerializeField]
    [Range(0, 1f)]
    private float forestChance, mountainChance; 

    protected override void Awake () {
        base.Awake();
        CreateMap(); 
	}

    private void CreateMap()
    {
        squarePrefab.transform.localScale = new Vector3(squareSize, squareSize, 1);
       
        Square[,] squares = new Square[mapSize.x, mapSize.y];

        float positionX = this.transform.position.x - ((mapSize.x*squareSize) / 2f) + squareSize / 2;
        float positionY = this.transform.position.y - ((mapSize.y*squareSize) / 2f) + squareSize / 2;

        float originalY = positionY;
        for (int i = 0; i < mapSize.x; i++)
        {
            for (int j = 0; j < mapSize.y; j++)
            {

                var aux = (GameObject)Instantiate(squarePrefab, new Vector3(positionX, positionY), Quaternion.identity);
                Square square = aux.GetComponent<Square>();
                square.InitSquare(new Coordinate(i, j));
                squares[i, j] = square;
                positionY += squareSize;
            }
            positionX += squareSize;
            positionY = originalY;
        }

        CellularGenerationMap(squares);

    }


    public void CellularGenerationMap(Square[,] squares)
    {
        // Random generation
        for (int i = 0; i< squares.GetLength(0); i++)
        {
            for (int j = 0; j < squares.GetLength(1); j++)
            {
                float randSeed = Random.Range(0, 1f);
                if (randSeed < forestChance)
                {
                    squares[i, j].Type = SquareType.FOREST; 
                }
                else if (randSeed < mountainChance)
                {
                    squares[i, j].Type = SquareType.MOUNTAIN;
                }
                else
                {
                    squares[i, j].Type = SquareType.NORMAL;
                }
            }
        }

        Square[,] newSquares = squares; 
        for (int iteration = 0; iteration < iterations; iteration++)
        {
            Square[,] oldMap = squares; 
            for (int i = 0; i < squares.GetLength(0); i++)
            {
                for (int j = 0; j < squares.GetLength(1); j++)
                {
                    int neighboursForest = 0;
                    int neighboursMountain = 0; 
                    
                    if (i-1 >= 0)
                    {
                        neighboursForest += oldMap[i - 1, j].Type == SquareType.FOREST ? 1 : 0;
                        neighboursMountain += oldMap[i - 1, j].Type == SquareType.MOUNTAIN ? 1 : 0;
                        if (j-1 >= 0)
                        {
                            neighboursForest += oldMap[i - 1, j-1].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i - 1, j-1].Type == SquareType.MOUNTAIN ? 1 : 0;
                            neighboursForest += oldMap[i , j-1].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i , j-1].Type == SquareType.MOUNTAIN ? 1 : 0;
                        }

                        if (j+1 < squares.GetLength(1))
                        {
                            neighboursForest += oldMap[i - 1, j +1 ].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i - 1, j + 1].Type == SquareType.MOUNTAIN ? 1 : 0;
                            neighboursForest += oldMap[i, j + 1].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i, j + 1].Type == SquareType.MOUNTAIN ? 1 : 0;

                        }
                    }

                    if (i + 1 < squares.GetLength(0))
                    {
                        neighboursForest += oldMap[i + 1, j].Type == SquareType.FOREST ? 1 : 0;
                        neighboursMountain += oldMap[i + 1, j].Type == SquareType.MOUNTAIN ? 1 : 0;
                        if (j + 1 < squares.GetLength(1))
                        {
                            neighboursForest += oldMap[i + 1, j + 1].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i + 1, j + 1].Type == SquareType.MOUNTAIN ? 1 : 0;
                        }

                        if (j - 1 >= 0)
                        {
                            neighboursForest += oldMap[i + 1, j - 1].Type == SquareType.FOREST ? 1 : 0;
                            neighboursMountain += oldMap[i + 1, j - 1].Type == SquareType.MOUNTAIN ? 1 : 0;
                        }
                    }

                    if ((neighboursForest < minLimitForest || neighboursForest > maxLimitForest) && (neighboursMountain < minLimitMountain || neighboursMountain > maxLimitMountain))
                    {
                        newSquares[i, j].Type = SquareType.NORMAL;
                    }
                    else if (neighboursForest > minLimitForest && neighboursForest < maxLimitForest)
                    {
                        newSquares[i, j].Type = SquareType.FOREST;
                    }

                    else if (neighboursMountain > minLimitMountain && neighboursMountain < maxLimitMountain)
                    {
                        newSquares[i, j].Type = SquareType.MOUNTAIN;
                    }
                    else{
                        newSquares[i, j].Type = SquareType.NORMAL; 
                    }

                }
            }
        }


        for (int i = 0; i< squares.GetLength(0); i++)
        {
            for (int j = 0; j< squares.GetLength(1); j++)
            {
                newSquares[i, j].SetSpriteType(); 
            }
        }



        GameController.Instance.Board = new Board(newSquares);

    }

    void Update () {
		
	}


    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(this.transform.position, new Vector3(mapSize.x * squareSize, mapSize.y * squareSize, 1));
    }

    public float SquareSize
    {
        get
        {
            return squareSize;
        }

    }

}




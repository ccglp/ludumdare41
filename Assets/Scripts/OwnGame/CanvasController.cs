﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro; 

public class CanvasController : Singleton<CanvasController> {
    [SerializeField]
    private CanvasGroup startPanel; 
    [SerializeField]
    private CanvasGroup actionPanel,interTurnPanel,attackPanel,popup;
    [SerializeField]
    private float animationFadeTime = 0.5f; 
 
    private GameObject attackAction;
    [SerializeField]
    private TextMeshProUGUI textAttackPhase; 

    [Header("TOP BAR")]
    [SerializeField]
    private TextMeshProUGUI charName;
    [SerializeField]
    private TextMeshProUGUI lifeText, squareText,squareDefendText;
    [SerializeField]
    private Image charImage, fillBar, squareImage;

    [Header("End Game Panel")]
    [SerializeField]
    private CanvasGroup endGameGroup;
    [SerializeField]
    private TextMeshProUGUI endText;

    private bool gameStarted = false; 

	void Start () {
        attackAction = GameObject.Find("Attack"); 
	}
    private void Update()
    {
        if (!gameStarted)
        {
            if (Input.GetMouseButtonDown(0))
            {
                HideStartGame();
                gameStarted = true;
                if (Constants.firstGame)
                {
                    DOVirtual.DelayedCall(3, () =>
                    {
                        ShowPopup(); 
                    });
                }
            }
        }

        if (popup.alpha == 1)
        {
            if (Input.GetButtonDown("Jump"))
            {
                HidePopUp(); 
            }
        }
    }



    public void ShowPopup()
    {
        popup.transform.localScale = Vector3.zero;
        popup.alpha = 1;
        popup.interactable = true;
        popup.blocksRaycasts = true;
        popup.transform.DOScale(1, animationFadeTime);
    }

    public void HidePopUp()
    {
        popup.DOFade(0, animationFadeTime);
        popup.interactable = false;
        popup.blocksRaycasts = false;
        Constants.firstGame = false; 
    }

    private void HideStartGame()
    {
        startPanel.DOFade(0, animationFadeTime);
        startPanel.interactable = false;
        startPanel.blocksRaycasts = false; 
    }

    public void FadeInAndOutInterTurnPanel(bool aiTurn)
    {

        interTurnPanel.GetComponentInChildren<TextMeshProUGUI>().text = aiTurn ? "AI TURN" : "NEXT TURN!";
        interTurnPanel.DOFade(1, animationFadeTime).onComplete = () =>
        {
            DOVirtual.DelayedCall(animationFadeTime * 5, () =>
            {
                interTurnPanel.DOFade(0, animationFadeTime); 
            });
        };
    }


    public void ShowActionPanel(bool canAttack)
    {
        attackAction.SetActive(canAttack);
        actionPanel.DOFade(1, animationFadeTime);
        actionPanel.interactable = true;
        actionPanel.blocksRaycasts = true; 
    }

    public void HideActionPanel()
    {
        actionPanel.DOFade(0, animationFadeTime);
        actionPanel.interactable = false;
        actionPanel.blocksRaycasts = false;
    }

    public void ShowAttackPanel(bool aiTurn)
    {
        attackPanel.alpha = 1;
        textAttackPhase.text = aiTurn ? "Defense Phase!" : "Attack Phase!"; 
        attackPanel.transform.localScale = Vector3.zero;
        attackPanel.transform.DOScale(1, animationFadeTime / 1.4f);
        //attackPanel.DOFade(1, animationFadeTime); 
    }
    public void HideAttackPanel(TweenCallback onEnd)
    {
        attackPanel.DOFade(0, animationFadeTime).onComplete = onEnd; 
    }


    public void SetTopBar(SpriteRenderer charImage, string charName, int lifeTotal, int lifeRest, SquareType type, Sprite squareSprite)
    {
        this.charImage.sprite = charImage.sprite;
        this.charImage.color = charImage.color; 
        this.charName.text = charName;
        this.lifeText.text = lifeRest.ToString() + "/" + lifeTotal.ToString();
        this.fillBar.fillAmount = (float)lifeRest / lifeTotal;

        this.squareImage.sprite = squareSprite;
        this.squareText.text = type.ToString();
        this.squareDefendText.text = ((int) type).ToString();
    }


    public void ShowEndGamePanel(string text)
    {
        endText.text = text;
        endGameGroup.DOFade(1, animationFadeTime);
        endGameGroup.interactable = true;
        endGameGroup.blocksRaycasts = true; 
    }
}

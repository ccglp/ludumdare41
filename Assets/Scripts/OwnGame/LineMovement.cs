﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineMovement : Singleton<LineMovement> {

    private LineRenderer line;
    private GameObject finalTriangle; 

    void Start () {
        finalTriangle = this.transform.GetChild(0).gameObject;
        finalTriangle.SetActive(false); 
        line = this.GetComponent<LineRenderer>();
        line.positionCount = 0; 
	}
	

    public void StartTrackingMovement(Vector3 position)
    {
        line.positionCount = 1; 
        line.SetPosition(0, position);
    }


    public void AddPointToTheMovement(Vector3 position)
    {
        line.positionCount++;
        line.SetPosition(line.positionCount - 1, position);
        finalTriangle.transform.position = position; 
        finalTriangle.SetActive(true); 
        Vector3 direction = line.GetPosition(line.positionCount - 1) - line.GetPosition(line.positionCount - 2);
        direction.Normalize(); 
        if (Vector3.Distance(direction, Vector3.right) < 0.1f)
        {
            finalTriangle.transform.eulerAngles = new Vector3(0, 0, -90); 
        }
        else if (Vector3.Distance(direction, Vector3.left) < 0.1f)
        {
            finalTriangle.transform.eulerAngles = new Vector3(0, 0, 90);
        }
        else if (Vector3.Distance(direction, Vector3.down)< 0.1f)
        {
            finalTriangle.transform.eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            finalTriangle.transform.eulerAngles = new Vector3(0, 0, 0); 
        }
    }

    public void RemovePointFromTheMovement()
    {
        line.positionCount--; 
    }

    public void EndTracking()
    {
        finalTriangle.SetActive(false);
        line.positionCount = 0; 
    }

	void Update () {
		
	}
}

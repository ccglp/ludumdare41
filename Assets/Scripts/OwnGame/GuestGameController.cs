﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuestGameController : MonoBehaviour {
    protected int attackPoints = 0; 


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected void SendAttackPoints()
    {
        GameController.Instance.CallbackFromAttack(attackPoints); 
        Destroy(this.gameObject);
    }
}

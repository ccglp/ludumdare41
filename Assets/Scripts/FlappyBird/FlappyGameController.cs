﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyGameController : GuestGameController {

    [SerializeField]
    private AnimationCurve spawnTime; 
    [SerializeField]
    GameObject baddyPrefab;
    [SerializeField]
    private GameObject plusOne;
    private float spawnSite = 13;
    private Vector2 limitUp = new Vector2(1.4f, 4);
    private Vector2 limitDown = new Vector2(-1.4f, -4);
    private bool gameStart = false;
    private float timer = 0;
    private float timerTotal = 0; 

	// Use this for initialization
	void Start () {
		
	}

    public void EndGame()
    {
        SendAttackPoints(); 
    }

    public void CallIncrementAttackPoints(Vector3 position)
    {
        SoundManager.Instance.PlayCoin(); 
        attackPoints++;
        Instantiate(plusOne, position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Jump"))
        {
            gameStart = true; 
        }
        if (gameStart)
        {
            timer += Time.deltaTime;
            timerTotal += Time.deltaTime; 
            if (timer > spawnTime.Evaluate(timerTotal))
            {
                timer = 0; 
                var up = (GameObject) Instantiate(baddyPrefab,this.transform.position + Vector3.right * spawnSite + Vector3.up * Random.Range(limitUp.x, limitUp.y), Quaternion.identity,this.gameObject.transform);
                var down = (GameObject) Instantiate(baddyPrefab, this.transform.position + Vector3.right * spawnSite + Vector3.up * Random.Range(limitDown.x, limitDown.y), Quaternion.identity, this.gameObject.transform);
                up.GetComponent<Rigidbody2D>().velocity = Vector2.left * 10;
                down.GetComponent<Rigidbody2D>().velocity = Vector2.left * 10; 
               
            }
        }
	}
}

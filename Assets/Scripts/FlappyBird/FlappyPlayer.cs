﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyPlayer : MonoBehaviour {
    private Rigidbody2D rb;
    [SerializeField]
    private float jumpSpeed, originalGravity = 1.3f, gravityChange = 1.8f;
    [SerializeField]
    private LayerMask layerMask;
    private int previousID = 0;

    private GameObject spriteObject; 
	// Use this for initialization
	void Start () {
        this.rb = this.GetComponent<Rigidbody2D>();
        this.spriteObject = this.transform.GetChild(0).gameObject; 
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Jump"))
        {
            SoundManager.Instance.PlayJump(); 
            rb.gravityScale = originalGravity;
            rb.velocity = new Vector2(0, jumpSpeed);
            this.spriteObject.transform.eulerAngles = new Vector3(0, 0, 35);
        }

        if (rb.velocity.y < -0.01)
        {
            rb.gravityScale = gravityChange;
            this.spriteObject.transform.eulerAngles = new Vector3(0, 0, -35); 
        }

        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.up,10,layerMask,-10); 
        if (hit.collider != null)
        {
           
            if (hit.collider.gameObject.tag == "Bad" && hit.collider.gameObject.GetInstanceID() != previousID)
            {
                previousID = hit.collider.gameObject.GetInstanceID(); 
                
                GameObject.FindObjectOfType<FlappyGameController>().CallIncrementAttackPoints(this.transform.position); 
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bad" || collision.gameObject.tag =="Die")
        {
            GameObject.FindObjectOfType<FlappyGameController>().EndGame(); 
        }
    }
}
